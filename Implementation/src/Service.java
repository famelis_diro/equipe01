import java.util.ArrayList;

/**
 * @author Mathieu David-Babin, Eric Hudon Bujold, Samuel Michaud
 * Classe representant un service offert par #GYM
 */
public class Service {
	

	private String startingDate;
	private String endingDate;
	private String time;
	private ArrayList<String> recurrence;
	private int capacity;
	private String professional_id;
	private String service_id;
	private short price;
	private String comments;
	private ArrayList<Client> inscrits = new ArrayList<Client>();
	/**
	 * @param startingDate
	 * @param endingDate
	 * @param time
	 * @param recurrence
	 * @param capacity
	 * @param professional_id
	 * @param service_id
	 * @param price
	 * @param comments
	 * @param inscrits
	 * Constructeur de service, prenant en parametre tous les details reli�s au service � cr�er
	 */
	public Service(String startingDate, String endingDate, String time, ArrayList<String> recurrence,
			int capacity, String professional_id, String service_id, short price, String comments) {
		this.startingDate = startingDate;
		this.endingDate = endingDate;
		this.time = time;
		this.recurrence = recurrence;
		this.capacity = capacity;
		this.professional_id = professional_id;
		this.service_id = service_id;
		this.price = price;
		this.comments = comments;
	}
	
	/**
	 * @return the startingDate
	 */
	public String getStartingDate() {
		return startingDate;
	}
	/**
	 * @param startingDate the startingDate to set
	 */
	public void setStartingDate(String startingDate) {
		this.startingDate = startingDate;
	}
	/**
	 * @return the endingDate
	 */
	public String getEndingDate() {
		return endingDate;
	}
	/**
	 * @param endingDate the endingDate to set
	 */
	public void setEndingDate(String endingDate) {
		this.endingDate = endingDate;
	}
	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}
	/**
	 * @param time 
	 * the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}
	/**
	 * @return the recurrence
	 */
	public ArrayList<String> getRecurrence() {
		return recurrence;
	}
	/**
	 * @param recurrence 
	 * the recurrence to set
	 */
	public void setRecurrence(ArrayList<String> recurrence) {
		this.recurrence = recurrence;
	}
	/**
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity;
	}
	
	/**
	 * Fonction qui ajoute un client au service actuel
	 */
	public void addClient(Client c) {
		this.inscrits.add(c);
	}
	
	/**
	 * Fonction qui retourne la liste des inscrits au service actuel
	 */
	public void listeClients() {
		for(Client temp : inscrits) {
			System.out.println(temp);
		}
	}
	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	/**
	 * @return the professional_id
	 */
	public String getProfessional_id() {
		return professional_id;
	}
	/**
	 * @param professional_id the professional_id to set
	 */
	public void setProfessional_id(String professional_id) {
		this.professional_id = professional_id;
	}
	/**
	 * @return the service_id
	 */
	public String getService_id() {
		return service_id;
	}
	/**
	 * @param service_id the service_id to set
	 */
	public void setService_id(String service_id) {
		this.service_id = service_id;
	}
	/**
	 * @return the price
	 */
	public short getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(short price) {
		this.price = price;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	/**
	 * Fonction qui retourne toutes les infos en lien avec le service, formatt� afin
	 * d'�tre lisible � l'�cran
	 */
	public String toString() {
		/*
		 * 	Date et heure actuelles (JJ-MM-AAAA HH:MM:SS)
			Date de d�but du service (JJ-MM-AAAA)
			Date de fin du service (JJ-MM-AAAA)
			Heure du service (HH:MM)
			R�currence hebdomadaire du service (quels jours il est offert � la m�me heure)
			Capacit� maximale (maximum 30 inscriptions)
			Num�ro du professionnel (9 chiffres)
			Code du service (7 chiffres)
			Frais du service (jusqu'� 100.00$)
			Commentaires (100 caract�res) (facultatif).

		 */
		String s = "Date de d�but de service: " + this.startingDate + "\n"
				+ "Date de fin de service: " + this.endingDate + "\n"
				+ "Heure du service: " + this.time + "\n"
				+ "R�currence hebdomadaire du service: " + String.join(",", this.recurrence) + "\n"
				+ "Capacit� maximale: " + this.capacity + "\n"
				+ "Num�ro du professionnel:" + this.professional_id + "\n"
				+ "Code du service:" + this.service_id + "\n"
				+ "Frais du service:" + this.price + "\n"
				+ "Commentaires: " + this.comments + "\n";
		
		
		return s;
	}
	

}
