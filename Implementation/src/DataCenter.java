import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Classe representant le centre de donn�es, les donn�es y sont stock�es
 * @author Mathieu David-Babin, Eric Hudon Bujold, Samuel Michaud
 */

public class DataCenter {
	
	private Map<String,Utilisateur> users;
	private ArrayList<Service> services;
	private int id;
	private int service;
	/**
	 * Constructeur initialise une hashmap et un arraylist pour stocker les users/services
	 */
	public DataCenter() {
		users = new HashMap<>();
		services = new ArrayList<Service>();
		setId(0);
		setService(0);
	}
	
	/**
	 * Fonction qui ajoute un client � la base de donn�es
	 * @param client
	 */
	public void addClient(Client client) {
		this.users.put(client.getId(),client);
		this.id++;
		
	}
	
	/**
	 * Fonction qui ajoute un professionnel � la base de donn�es
	 * @param professional
	 */
	public void addProfessional(Professionnel professional) {
		this.users.put(professional.getId(),professional);
		this.id++;
	}
	
	/**
	 * Fonction qui ajoute un service � la base de donn�es
	 * @param service
	 */
	public void addService(Service service) {
		this.services.add(service);
		this.service++;
	}
	
	/**
	 * Fonction qui retourne le client correspondant au ID fourni
	 * @param id
	 * @returna
	 */
	public Client getClient(String id) {
		return (Client) users.get(id);
	}
	
	/**
	 * Fonction qui retourne le professionnel correspondant au ID fourni
	 * @param id
	 * @return
	 */
	public Professionnel getProfessional(String id) {
		return (Professionnel)users.get(id);
	}
	
	/**
	 * Fonction qui retourne le service correspondant au ID fourni
	 * @param id
	 * @return
	 */
	public Service getService(String id) {
		return services.get(Integer.parseInt(id));
	}
	
	/**
	 * Fonction qui retourne la liste complete des utilisateurs
	 * @return users
	 */
	public Map<String,Utilisateur> getUsers() {
		return users;
	}
	/**
	 * Fonction pour sauvegarder une hashmap contenant les clients
	 * @param users
	 */
	public void setUsers(Map<String,Utilisateur> users) {
		this.users = users;
	}
	/**
	 * Fonction getter pour obetnir la liste des services fournis par le gym
	 * @return services
	 */
	public ArrayList<Service> getServices() {
		return services;
	}
	/**
	 * Fonction pour sauvegarder une arrayList contenant les services fournis
	 * @param services
	 */
	public void setServices(ArrayList<Service> services) {
		this.services = services;
	}
	
	/**
	 * Fonction pour retirer un utilisateur a l'aide de son ID
	 * @param id
	 */
	public void removeUser(String id) {
		Utilisateur u = this.users.remove(id);
		if(u == null)
			throw new NullPointerException("Le ID sp�cifi� n'existe pas !");
	}
	
	/**
	 * Fonction pour retirer un service a l'aide de son ID
	 * @param id
	 */
	public void removeService(String id) {
		int i = 0;
		for(Service temp : this.services) {
			if(temp.getProfessional_id()==id) {
				this.services.remove(i);
				break;
			}
			i++;
		}
	}

	/**
	 * Fonction qui genere le ID du prochain user
	 * @return id
	 */
	public String getNextID() {
		return genId(this.id);
	}
	
	/**
	 * Fonction getter pour obtenir le ID en String format�(ajout de 0 a gauche)
	 * @param i
	 * @return
	 */
	public String getIdToString(int i) {
		return genId(i);
	}
	
	/**
	 * Fonction getter pour obtenir le service en String format�(ajout de 0 a gauche)
	 * @param i
	 * @return
	 */
	public String getServiceToString(int i) {
		return genService(i);
	}
	
	/**
	 * Fonction retournant le ID qui sera associ� au prochain service
	 * @return
	 */
	public String getService() {
		return genService(this.service);
	}

	/**
	 * Fonction pour initialiser le numero de ID
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * TODO
	 * Fonction pour initialiser le numero des services
	 * @param service
	 */
	public void setService(int service) {
		this.id = service;
	}
	
	/**
	 * Ajout de 0 a gauche(9 chiffres totaux)
	 * @param id
	 * @return
	 */
	private String genId(int id) {
		String t = String.valueOf(id);
		int diff = 9-t.length();
		String p = "";
		for(int i =0;i<diff;i++) {
			p += "0";
		}
		return p+t;
	}
	
	/**
	 * Ajout de 0 a gauche(7 chiffres totaux)
	 * @param service
	 * @return
	 */
	private String genService(int service) {
		String t = String.valueOf(service);
		int diff = 7-t.length();
		String p = "";
		for(int i =0;i<diff;i++) {
			p += "0";
		}
		return p+t;
	}
}
