
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * @author Samuel Michaud, Mathieu David-Babin, Eric Hudon-Bujold 
 * Classe vue avec laquelle l'agent interagit pour effectuer differentes operations de #GYM
 */
public class Vue {

	private DataCenter dc;
	/**
	 * @see #Vue()
	 * Constructeur de la vue
	 */
	public Vue() {
		// TODO Auto-generated constructor stub
		this.setDc(new DataCenter());

		bouclePrincipale:
			while(true){
				try {
					System.out.println("Menu principal\n"
							+ "S�lectionnez un service : \n"
							+ "***************************\n"
							+ "1. Ajouter/supprimer/modifier client\n"
							+ "2. Inscription s�ance \n"
							+ "3. Ajouter/supprimer/modifier s�ance\n"
							+ "4. Authentifier un membre\n"
							+ "5. Consulter inscription aux s�ances\n"
							+ "6. Acc�s au r�pertoire service\n"
							+ "7. Confirmation pr�sence\n"
							+ "8. Proc�dure comptable\n"
							+ "9. Production de rapport de synth�se\n"
							+ "10. Quitter");
					Scanner sc = new Scanner(System.in);
					String i = sc.nextLine();
					switch(i) {
					case "1": gererMembre();
					break;
					case "2": inscrireSeance();
					break;
					case "3": gererSeance();
					break;
					case "4": authentification();
					break;
					case "5": consulterInscription();
					break;
					case "6": accederServices();
					break;
					case "7": confirmerPresence();
					break;
					case "8": procedureComptable();
					break;
					case "9": rapportSynthese();
					break;
					case "10": break bouclePrincipale;
					default:
						System.out.println("Nombre invalide !");
						continue;
					}
				}
				catch(InputMismatchException e) {
					System.out.println("Saisie invalide !");
				}
			}
	}
	/**
	 * Fonction qui permet a l'agent d'ajouter, supprimer ou modifier un client chez #GYM
	 */
	private void gererMembre() {
		System.out.println("Choisissez une option :\n"
				+ "1. Ajouter membre\n"
				+ "2. Modifier membre\n"
				+ "3. Supprimer membre\n"
				+ "4. Retour au menu principal");
		Scanner f = new Scanner(System.in);
		String i = f.nextLine();
		switch(i) {
			case "1": ajouterMembre();
			break;
			case "2": modifierMembre();
			break;
			case "3": retirerMembre();
			break;
			case "4": break;
			default: System.out.println("Commande invalide!");
			break;
		}
	}
	/**
	 * Fonction qui permet a l'agent d'ajouter, supprimer ou modifier un service chez #GYM
	 */
	private void gererSeance() {
		System.out.println("Choisissez une option :\n"
				+ "1. Ajouter une s�ance\n"
				+ "2. Modifier une s�ance\n"
				+ "3. Supprimer une s�ance\n"
				+ "4. Retour au menu principal");
		Scanner f = new Scanner(System.in);
		String i = f.nextLine();
		switch(i) {
		case "1": ajouterService();
		break;
		case "2": modifierService();
		break;
		case "3": retirerService();
		break;
		case "4": break;
		default: System.out.println("Commande invalide!");
		break;
		}
	}
	/**
	 * Fonction qui permet a l'agent de verifier que le membre est inscrit 
	 * avec son numero d'usager unique
	 */
	private void authentification() {
		String id;
		Scanner f = new Scanner(System.in);
		do {
			System.out.println("Entrez le num�ro de l'utilisateur � valider:");	
			id = f.nextLine();
			if(id.length() != 9) {System.out.println("Num�ro invalide. Le num�ro doit �tre compos� de 9 charact�res");}
		}while(id.length() != 9);
		if(dc.getClient(id) != null) {
			System.out.println(dc.getClient(id));
		}
		else
			System.out.println("Le ID sp�cifi� n'est associ� a aucun membre de #GYM!");
	}
	/**
	 * Fonction qui permet a l'agent de verifier les inscriptions a 
	 * un cours grace a l'ID correspondant
	 */
	private void consulterInscription() {
		String noService;
		Scanner f = new Scanner(System.in);
		do {
			System.out.println("Entrez le num�ro du service d�sir�:");	
			noService = f.nextLine();
			if(noService.length() != 7) {System.out.println("Num�ro invalide. Le num�ro doit �tre compos� de 7 charact�res");}
		}while(noService.length() != 7);
		try {
			dc.getService(noService).listeClients();
		}
		catch(NullPointerException e) {
			System.out.println("Le service sp�cifi� n'existe pas !");
		}
	}
	
	/**
	 * Fonction qui permet a l'agent de verifier quels sont les services offerts
	 */
	private void accederServices() {
		for(Service temp : dc.getServices()) {
			System.out.println(temp);
		}	
	}
	
	/**
	 * Fonction qui permet a l'agent de confirmer la presence du 
	 * client a une seance a laquelle il s'est inscrit
	 */
	private void confirmerPresence() {
		
	}
	/**
	 * Procedure qui genere les TEFs
	 */
	private void procedureComptable() {
	
	}
	
	/**
	 * Procedure qui genere le rapport de synthese, enclench� 
	 * automatiquement avec la procedure comptable OU par l'admin en cas de besoin 
	 */
	private void rapportSynthese() {
		System.out.println("Rapport synth�se\n"
				+ "**************************");
		
	}	
	/**
	 * Fonction qui permet a l'admin d'inscrire un usager a une seance a l'aide de son ID usager
	 */
	private void inscrireSeance() {
		String noService, idClient;
		Scanner f = new Scanner(System.in);
		do {
			System.out.println("Entrez le num�ro du service d�sir�:");	
			noService = f.nextLine();
			if(noService.length() != 7) {System.out.println("Num�ro invalide. Le num�ro doit �tre compos� de 7 charact�res");}
		}while(noService.length() != 7);
		do {
			System.out.println("Entrez le num�ro du client � inscrire:");	
			idClient = f.nextLine();
			if(idClient.length() != 9) {System.out.println("Num�ro invalide. Le num�ro doit �tre compos� de 9 charact�res");}
		}while(idClient.length() != 9);
		try {
			dc.getService(noService).addClient(dc.getClient(idClient));
			System.out.println("L'inscription est confirm�e!");
		}
		catch(NullPointerException e) {
			System.out.println("Le service ou client sp�cifi� est invalide!");
		}
	}
	/**
	 * Fonction contenant les operations d'ajout de membre
	 */
	private void ajouterMembre() {
		Scanner f = new Scanner(System.in);
		String choix;
		do {
			System.out.println("Souhaitez vous ajouter un client(1) ou un professionnel(2) ?");
			choix = f.nextLine().trim();
		} while (choix.compareTo("1") != 0 && choix.compareTo("2") != 0);
		System.out.println("Entrez votre nom : ");
		String nom = f.nextLine();
		System.out.println("Entrez votre date de naissance (JJ/MM/AAAA) : ");
		String naissance = f.nextLine();
		System.out.println("Entrez votre numero de telephone ((123)-456-7890)");
		String phone = f.nextLine();
		System.out.println("Entrez votre adresse courriel");
		String mail = f.nextLine();
		String id = this.dc.getNextID();
		if(choix.compareTo("1") == 0) {
			this.dc.addClient(new Client(naissance,id,nom,phone, mail));
			System.out.println(this.dc.getClient(id).toString());
		}
		else {
			this.dc.addProfessional(new Professionnel(naissance, id, nom, phone, mail));		
			System.out.println(this.dc.getProfessional(id).toString());
		}
	}

	/**
	 * Fonction contenant les operations pour modifier un membre existant
	 */
	private void modifierMembre(){
		Scanner f = new Scanner(System.in);
		System.out.println("Le membre � modifier est-il un client(1) ou un professionnel(2)?");
		String type = f.nextLine().trim();
		String user = (type.compareTo("1")==0)?"client":"professionel";
		String id;
		do {
			System.out.println("Entrez le num�ro du "+user+" � modifier");	
			id = f.nextLine();
			if(id.length() != 9) {System.out.println("Num�ro invalide. Le num�ro doit �tre compos� de 9 charact�res");}
		}while(id.length() != 9);
		System.out.println("Voici les informations disponibles:");
		Utilisateur c;
		if(type.compareTo("1")==0) {
			c = this.dc.getClient(id);
			System.out.println(c.toString());
		}
		else {
			c = this.dc.getProfessional(id);
			System.out.println(c);
		}
		boucleModifUser:
			while(true) {
				System.out.println("Quelle information d�sirez-vous modifier ?\n"
						+ "1- Nom\n"
						+ "2- Date de naissance\n"
						+ "3- T�l�phone\n"
						+ "4- Adresse couriel\n"
						+ "5- Arr�ter la modification");
				String i = f.nextLine();
				switch(i) {
				case "1": System.out.println("Indiquer le nouveau nom: ");
				String n = f.nextLine();
				c.setName(n);
				break;
				case "2": System.out.println("Indiquer la nouvelle date de naissance: ");
				c.setBirth(f.nextLine());
				break;
				case "3": System.out.println("Indiquer le nouveau num�ro de t�l�phone: ");
				c.setPhone(f.nextLine());
				break;
				case "4": System.out.println("Indiquer le nouveau courriel");
				c.setMail(f.nextLine());
				break;
				case "5":
					break boucleModifUser;
				default: System.out.println("Commande invalide!");
				break;
				}
				System.out.println("Nouvelles informations: \n"+c);
			}
	}
	/**
	 * Fonction qui contient les operations pour retirer un membre existant
	 */
	private void retirerMembre() {
		Scanner f = new Scanner(System.in);
		System.out.println("Le membre � retirer est-il un client(1) ou un professionnel(2)?");
		String type = f.nextLine().trim();
		String user = (type.compareTo("1")==0)?"client":"professionel";
		String id;
		do {
			do {
				System.out.println("Entrez le num�ro du "+user+" � retirer");	
				id = f.nextLine();
				if(id.length() != 9) {System.out.println("Num�ro invalide. Le num�ro doit �tre compos� de 9 charact�res");}
			}while(id.length() != 9);
			try {
				this.dc.removeUser(id);
				System.out.println("Utilisateur supprim�");	
				break;
			}
			catch(NullPointerException e) {
				System.out.println("");
			}
		}while(true);
	}
	/**
	 * Fonction contenant les operations pour ajouter un service 
	 */
	private void ajouterService() {
		Scanner f = new Scanner(System.in);
		//D�but de service
		System.out.println("Entrez la date de d�but de service (JJ-MM-AAAA) : ");
		String debut = f.nextLine();
		//Fin de service
		System.out.println("Entrez la date de fin de service (JJ-MM-AAAA)");
		String fin = f.nextLine();
		//Heure du service
		System.out.println("Entrez l'heure du service (HH:MM)");
		String heure = f.nextLine();
		//R�currence
		System.out.println("Entrez les jours o� le service est diponible (j,j,j)");
		ArrayList<String> recc = new ArrayList<String>();
		recc.addAll(Arrays.asList(f.nextLine().split(",")));
		//Capacit�
		int cap;
		do {
			System.out.println("Entrez la capacit� (maximum 30)");
			cap = Integer.parseInt(f.nextLine());
			if(cap>30) {System.out.println("Donn�e incoh�rente, veuillez r�essayer.");}
		}while(cap>30);
		//Num�ro de professionnel
		String proID;
		boolean idNonValide;
		do {
			idNonValide = true;
			System.out.println("Entrez le num�ro du professionnel (9 charact�res)");	
			proID = f.nextLine();
			if(proID.length() != 9) {System.out.println("Num�ro invalide. Le num�ro doit �tre compos� de 9 charact�res");}
			else if(this.dc.getProfessional(proID) == null) {
				System.out.println("Le professionnel n'existe pas !");
				idNonValide = false;
			}
		}while(proID.length() != 9 || idNonValide == false );
		
		//Prix
		short price;
		do {
			System.out.println("Entrez le prix du service (maximum 100$)");
			price = (short) Integer.parseInt(f.nextLine());
			if(price>100) {System.out.println("Donn�e incoh�rente, veuillez r�essayer.");}
		}while(price>100);
		//Commentaires
		System.out.println("Entrez des commentaires (facultatif)");
		String comments = f.nextLine();
		//Ajout du service
		String id = this.dc.getService();
		this.dc.addService(new Service(debut, fin, heure, recc, cap, proID, id , price, comments));
		System.out.println(this.dc.getServices().get(Integer.parseInt(id)).toString());
	}

	/**
	 * Fonction contenant les operations de modification pour un service existant
	 */
	private void modifierService() {
		Scanner f = new Scanner(System.in);
		String id;
		do {
			System.out.println("Entrez le num�ro du service � modifier");	
			id = f.nextLine();
			if(id.length() != 7) {System.out.println("Num�ro invalide. Le num�ro doit �tre compos� de 9 charact�res");}
		}while(id.length() != 7);
		System.out.println("Voici les informations disponibles:");
		Service s = dc.getService(id);
		boucleModifService:
			while(true) {
				System.out.println("Quelle information d�sirez-vous modifier ?\n"
						+ "1- Date de debut du service\n"
						+ "2- Date de fin du service\n"
						+ "3- Heure\n"
						+ "4- R�currence\n"
						+ "5- Capacit�\n"
						+ "6- ID du professionnel\n"
						+ "7- ID du service\n"
						+ "8- Prix\n"
						+ "9- Commentaire\n"
						+ "10- Arr�ter la modification");
				String i = f.nextLine();
				switch(i) {
					case "1": System.out.println("Indiquer la nouvelle de debut: ");
					s.setStartingDate(f.nextLine());
					break;
					case "2": System.out.println("Indiquer la nouvelle date de fin: ");
					s.setEndingDate(f.nextLine());
					break;
					case "3": System.out.println("Indiquer la nouvelle heure: ");
					s.setTime(f.nextLine());
					break;
					case "4": System.out.println("Indiquer la nouvelle r�currence:");
					//TODO s.setRecurrence(f.nextLine());
					break;
					case "5": System.out.println("Indiquer la nouvelle capacit�:");
					s.setCapacity(Integer.parseInt(f.nextLine()));
					break;
					case "6": System.out.println("Indiquer le nouvel ID professionnel:");
					s.setProfessional_id(f.nextLine());
					break;
					case "7": System.out.println("Indiquer le nouvel ID du service:");
					s.setService_id(f.nextLine());
					break;
					case "8": System.out.println("Indiquer le nouveau prix:");
					s.setPrice(Short.parseShort(f.nextLine()));
					break;
					case "9": System.out.println("Indiquer le nouveau commentaire:");
					s.setComments(f.nextLine());
					break;
					case "10":
						break boucleModifService;
					default: System.out.println("Commande invalide!");
					break;
				}
				System.out.println("Nouvelles informations: \n"+s);
			}
	}
	
	/**
	 * Fonction contenant les operations pour retirer un service existant
	 */
	private void retirerService() {
		String id;
		Scanner f = new Scanner(System.in);
		do {
			do {
				System.out.println("Entrez le num�ro du service � supprimer");	
				id = f.nextLine();
				if(id.length() != 7) {System.out.println("Num�ro invalide. Le num�ro doit �tre compos� de 9 charact�res");}
			}while(id.length() != 7);
			try {
			dc.removeService(id);
			System.out.println("Le service a �t� retir� !");
			break;
			}
			catch(NullPointerException e) {
				System.out.println("Le ID sp�cifi� n'existe pas !");
			}
		}while(true);
	}

	/**
	 * Fonction getter pour le DataCenter associ� � la vue
	 * @return
	 */
	public DataCenter getDc() {
		return dc;
	}
	/**
	 * Fonction setter pour le DataCenter associ� � la vue
	 * @param dc
	 */
	public void setDc(DataCenter dc) {
		this.dc = dc;
	}
}