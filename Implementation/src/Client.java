/**
 * Classe representant un client, herite de Utilisateur
 * @author Samuel Michaud, Mathieu David-Babin, Eric Hudon-Bujold
 */
public class Client extends Utilisateur {
	private boolean membership_state = false;
	/**
	 * @param birth
	 * @param id
	 * @param name
	 * @param phone
	 * @param mail
	 * Constructeur qui prend les informations en parametre et appelle le constructeur de
	 * utilisateur pour instancier le client
	 */
	public Client(String birth, String id, String name, String phone, String mail) {
		super(birth,id,name,phone,mail);
	}
	
	/**
	 * @return the membership_state
	 */
	public boolean hasPaid() {
		return membership_state;
	}
	/**
	 * @param membership_state the membership_state to set
	 */
	public void setMembership_state(boolean membership_state) {
		this.membership_state = membership_state;
	}
	
	/**
	 * Appel de toString() pour afficher les infos du client format�es
	 */
	public String toString() {
					
		return super.toString();
	}
}
