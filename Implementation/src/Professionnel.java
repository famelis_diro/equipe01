import java.util.ArrayList;

/**
 * Classe representant un professionnel, herite de Utilisateur
 * @author Mathieu David-Babin, Eric Hudon Bujold, Samuel Michaud
 */
public class Professionnel extends Utilisateur {
	private ArrayList<Service> services;
	
	/**
	 * @param birth 
	 * @param name 
	 * @param id 
	 * @param phone 
	 * @param mail 
	 * Constructeur qui prend les informations en parametre et appelle le constructeur de
	 * utilisateur pour instancier le professionnel
	 */
	public Professionnel(String birth, String id, String name, String phone, String mail) {
		super(birth, id, name, phone, mail);
	}
	
	/**
	 * @return the services
	 */
	public ArrayList<Service> getServices() {
		return services;
	}
	/**
	 * @param services the services to set
	 */
	public void addService(Service service) {
		this.services.add(service);
	}

}
