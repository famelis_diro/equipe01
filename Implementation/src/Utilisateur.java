/**
 * @author Mathieu David-Babin, Eric Hudon Bujold, Samuel Michaud
 * Classe representant un utilisateur, parente de client et professionnel
 */

public abstract class Utilisateur {
	private String birth;
	private String id;
	private String phone;
	private String name;
	private String mail;
	
	/**
	 * @param birth
	 * @param id
	 * @param name
	 * @param phone
	 * @param mail
	 */
	public Utilisateur(String birth, String id, String name, String phone, String mail) {
		super();
		
		this.birth = birth;
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.mail = mail;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		
		this.id = id;
	}

	/**
	 * @return the birth
	 */
	public String getBirth() {
		return birth;
	}

	/**
	 * @param birth the birth to set
	 */
	public void setBirth(String birth) {
		this.birth = birth;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	/**
	 * Fonction qui imprime les informations de l'utilisateur formatt�es
	 */
	public String toString() {
		String s = "Id: " + this.id + "\n"
				+ "Nom: " + this.name + "\n"
				+ "Naissance: " + this.birth + "\n"
				+ "T�l�phone: " + this.phone + "\n"
				+ "Couriel: " + this.mail + "\n";
		return s;
		
	}
	
}
